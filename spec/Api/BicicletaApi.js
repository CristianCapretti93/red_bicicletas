let Bicicleta=require('../../models/bicicleta');
let request=require('request');
var server=require('../../bin/wwww')

describe('', ()=>{
    describe('Get Bicicleta', ()=>{
        it('status200', ()=>{
            expect(Bicicleta.allBicis).toBe(2);
            let bici= new Bicicleta('5','negro','modelo2',[-9090,-9093]);
            Bicicleta.add(bici);
            request.get('http://localhost:3000/api/bicicletas', function (error, response, bodd){

                expect(response.statusCode).toBe(200);
            }
            );
            
        })

    })
    describe('Post Bicicleta', (done)=>{
        it('status200', ()=>{
            expect(Bicicleta.allBicis).toBe(2);
            let bici= new Bicicleta('5','negro','modelo2',[-9090,-9093]);
            Bicicleta.add(bici);
            request.get('http://localhost:3000/api/bicicletas', function (error, response, bodd){

                expect(response.statusCode).toBe(200);
                expect(Bicicleta.encontrarporId(bici.id).id).toBe(5);
                done();
            }
            );
            
        })

    })
    
})