const Bicicleta= require('../../models/bicicleta');
var mongoose=require('mongoosee');
const bicicleta = require('../../models/bicicleta');


describe('Testing Bicicletas', function(){
   beforeEach(function(done)
   {
        var mongoDB='mongoDb://localhost/testdb'
        mongoose.connect(mongoDB,{useNewUrlParser:true})
        const db=mongoose.connection;
        db.on("Error", console.error.bind(console, "Conexion error"))
        db.once('open', function(){
            console.log('Se conecto a la Base de Datos');
            done();
        });
    });
    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, succes){
                if(err) console.log(err);
                done();
        })
    });


    describe('Crear instancia', ()=>{
        it('Crear una bicicletra', ()=>{
            var bici=Bicicleta.crearInstancia(1,"verde", "urbana", [-34.8833 ,-56.1667]);
            expect(bici.codigo).toBe(1);
            expect(bici.color).toBe('verde');
            expect(bici.modelo).toBe('urbana');
            expect(bici.ubicacion[0]).toEqual(-34.8833);
            expect(bici.ubicacion[1]).toEqual(-56.1667);
        })
    });
    describe('Bicicletra allbici', ()=>{
        it('comienza vacia', (done)=>{
           Bicicleta.allBicis(function(err,bicis){
                expect(bicis.length).toBe(0);
                done();
           });
        });
    });
    describe('Bicicletra.add', ()=>{
        it('Agregar una sola bici', (done )=>{
            var abici=new Bicicleta({code:1, color:'verde',
            modelo:'urbana', ubicacion:{}});
            bicicleta.add(abici, function(err, bici){
                if(err) console.log(err);
                Bicicleta.allBicis(function(err,bici){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(1);
                    done();
                })
            });
           
        });
    });
    describe('Bicicletra.add', ()=>{
        it('Agregar una sola bici', (done )=>{
           Bicicleta.allBicis(function(err,bicis){
               expect(bicis.length).toBe(0);
               var aBici= new Bicicleta({code:1, color:'verde',
               modelo:'urbana', ubicacion:{}});
               bicicleta.add(abici, function(err, bici){
                if(err) console.log(err);
                var aBici2= new Bicicleta({code:1, color:'verde',
               modelo:'urbana', ubicacion:{}});
               bicicleta.add(abici, function(err, bici2){
                if(err) console.log(err);
                Bicicleta.findByCode(1, function(error, targetBici){
                    expect(targetBici.code).toBe(abici.code);
                    expect(targetBici.color).toBe(abici.color);
                    expect(targetBici.modelo).toBe(abici.modelo);
                    done();
                })
                })
            });  
           })
           
        });
    });
})

