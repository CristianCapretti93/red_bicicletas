var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const passport=require('./config/passport')
const session=require('express-session');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/usuarios');
var bicicletasRouter=require('./routes/bicicletas');
var bicicletasApi=require('./routes/api/bibicletas')
var usuarioApi= require('./routes/api/usuario');
var tokenRouter=require('./routes/token');

//Se guarda la sesion en memoria
const store=session.MemoryStore


var app = express();
app.use(session(
  {
    cookie:{maxAge:240*60*60*2000}, // definir cuando dura la Cookie en este caso este valores corresponde a 10 dias 
    store: store,
    saveUninitialized:true,
    resave:true,
    secret:'red?Bicis-[0]/.-090-@039' //Codigo que genera la encriptacion
  }
))
var mongoose=require('mongoose');
const usuario = require('./models/usuario');
var mongoDb='mongodb://localhost/bicicletas'
mongoose.connect(mongoDb,{useNewUrlParser:true});
mongoose.Promise=global.Promise;
var db=mongoose.connection;
db.on('error', console.error.bind(console,'Error al conectarse'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.Initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/usuarios', usersRouter);
app.use('/bicicletas', bicicletasRouter);
app.use('/api/bicicletas', bicicletasApi);
app.use('/api/usuario', usuarioApi);
app.use('/token')

app.get("/loguin", function(req,res){
  res.render('session/loguin');
});
app.post("/loguin", function(req,res,next)
{
  passport.authenticate('local', function(err, usuario, info ){
    if(err) return next(err);
    if(!usuario)return res.render('session/loguin',{info})
    req.logIn(usuario, function(err){
      if(err) return next(err)
      return res.redirect("/")
    });
  })(req, res, next)
});
app.get("/logout", function(req,res){
 req.logOut();
  res.render('')
})

app.get("/forgotpassword",function(req,res){
  usuario.findOne({email:req.body.email}, function(err,usuario){
    if(!usuario) return res.render('sesion/forgotpassword', {info:{message:"No se encontro el usuario"}})

    usuario.resetpassword(function(err){
      if(err) return next(err)

    });
    res.render('session/forgotpassword')
  })

})
app.post("/forgotpassword",function(req,res,next){

})
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
