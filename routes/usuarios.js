var express = require('express');
var router=express.Router();
var usuarioController=require('../controlers/usuario');

router.get('/',usuarioController.list);
router.get('/create', usuarioController.create_get);
router.post('/create',usuarioController.create);
router.post('/:id/delete', usuarioController.delete);
router.get('/:id/update', usuarioController.update_get);
router.post('/:id/update',usuarioController.update);


module.exports=router;

module.exports = router;
