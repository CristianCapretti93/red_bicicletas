var express=require('express');
var router=express.Router();
var bicicletaController=require('../../controlers/api/bicicletaControllerApi');

router.get('/', bicicletaController.Bicicleta_Obtener);
router.post('/create', bicicletaController.Bicicleta_Crear);
router.delete('/:id/delete', bicicletaController.Bicicletas_Delete);
router.put('/:id/update', bicicletaController.Bicicletas_Update);

module.exports=router;