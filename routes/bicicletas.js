var express=require('express');
var router=express.Router();
var bicicletaController=require('../controlers/bicicleta');

router.get('/',bicicletaController.Bicicleta_list);
router.get('/create', bicicletaController.bicicleta_Create_get);
router.post('/create',bicicletaController.bicicleta_Create_post);
router.post('/:id/delete', bicicletaController.bicicletaDelete);
router.get('/:id/update', bicicletaController.bicicleta_Update_get);
router.post('/:id/update',bicicletaController.bicicleta_Update_post);


module.exports=router;


