let Bicicleta= require('../../models/bicicleta');

exports.Bicicleta_Obtener=function(req, res)
{
    res.status(200).json({
        bicicletas :Bicicleta.allBicis
    })
}
exports.Bicicleta_Crear=function(req, res){
    let bici= new Bicicleta(req.body.id, req.body.color, req.body.modelo)
    bici.ubicacion=[req.body.latitud, req.body.longitud];
    Bicicleta.add(bici);
    res.status(200).json({
        bicicletas: bici
    })
}
exports.Bicicletas_Delete=function(req, res)
{
      let bici=Bicicleta.Borrar(req.params.id);
      res.status(200).json({
          "Mensaje":"Operacion Exitosa"
      })
}

exports.Bicicletas_Update=function(req, res)
{
    let bici=Bicicleta.encontrarporId(req.params.id);
    bici.id=req.body.id;
    bici.color=req.body.color;
    bici.modelo=req.body.modelo;
    bici.ubicacion=[req.body.latitud, req.body.longitud];
    res.status(200).json({
        "Mensaje":"Operacion Exitosa",
        Bicicleta:bici
    })
}