var Token=require('../models/Token');
var Usuario=require('../models/usuario');
module.exports={

    confirmationget:function(req,res)
    {
        Token.findOne({token:req.params.token}, function(err, token){
            if(!token)
            {
                return res.status(400).send({type:'no verificado', message:'Token no ejeutado'})
            }
            Usuario.findById(token._userId, function(err, usuario){
                if(!usuario){return res.status(400).send({message:'No se encontro un usuario'})}
                if(usuario.verificado) return res.redirect('/usuarios')
                usuario.verificado=true;
                usuario.save(function(err){
                    if(err){return res.status(500).send({msg:err.message})}
                    res.redirect('/')
                })
            })
        })

    }

}