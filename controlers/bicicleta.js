var Bicicleta=require('../models/bicicleta');


exports.Bicicleta_list=function(req,res){

    res.render('bicicletas/index', {bicis:Bicicleta.allBicis});

}

exports.bicicleta_Create_get=function(req,res)
{   
    res.render('bicicletas/create');
}

exports.bicicleta_Create_post=function(req,res)
{   
    var bici= new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion=[req.body.latitud, req.body.long];
    Bicicleta.add(bici);
    res.redirect('/bicicletas');
}

exports.bicicletaDelete=function(req,res)
{
    var bici=Bicicleta.Borrar(req.body.id)
    res.redirect('/bicicletas');
}
exports.bicicleta_Update_get=function(req,res)
{   
    let bici=Bicicleta.encontrarporId(req.params.id);
    res.render('bicicletas/update', {bici});
}

exports.bicicleta_Update_post=function(req,res)
{   
    let bici=Bicicleta.encontrarporId(req.body.id);
    bici.id=req.body.id;
    bici.color=req.body.color;
    bici.modelo=req.body.modelo;
    bici.ubicacion=[req.body.latitud, req.body.long];
    res.redirect('/bicicletas');
}