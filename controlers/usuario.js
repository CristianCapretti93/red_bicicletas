const { response } = require('express');
var Usuario=require('../models/usuario');


module.exports={

    list:function(req, res, next)
    {
        Usuario.find({},(err, usuarios)=>{
            res.render('usuarios/index', {usuarios:usuarios})
        });
    },
    update_get:function(req,res, next)
    {
        Usuario.findById(req.body.id, function(err, usuario){
            res.render('usuarios/update', {errors:{}, usuario:usuario})
        })
    },
    update:function(req,res, next)
    {
        Usuario.findByIdAndUpdate(req.body.id,function(err, usuarios){
            if(err)
            {
                console.log(err)
                res.render('usuarios/update', {errors:err.errors, usuario: new Usuario({nombre:req.body.nombre, email:req.body.email})})
            }
            else
            {
                res.redirect('/usuarios');
                return;
            }
        })
    },
    delete:function(req,res, next)
    {
        Usuario.findByIdAndDelete(req.body.id, function(err){
            if(err)
                next();
            else
                res.redirect('/usuarios')    

        })
    },
    create:function(req,res, next)
    {   
        if(req.body.password != req.body.confirmpassword){
            res.render('usuarios/create', {errors:{confirm_password: {message:"No coincide con el pasword ingresado"}},
        usuario: new Usuario({nombre: req.body.name, email:req.body.email})})
            return;
        }
        Usuario.create({nombre:req.body.nombre,email:req.body.email, password:req.body.password })
        if(err)
        {
            res.render('usuarios/create',{errors: err.errors, usuario: new Usuario})
        }
        else
        {
            nuevoUsuario.enviar_email_bienvenida();
            res.redirect('/usuarios')
        }
    },
    create_get:function(req,res, next)
    {
        res.render('usuarios/create',{errors:{}, usuario:new Usuario()})
    }


    




}