var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var moment=require('moment');
var usuarioSchema=require('./usuario')
var reservaSchema=new Schema({  
    desde: Date,
    hasta: Date,
    bicicleta:{type:mongoose.Schema.Types.ObjectId, ref:'bicicleta'},
    usuario:{type:mongoose.Schema.Types.ObjectId, ref:'usuario'}
    //modelo: Sstring,
    //ubicacion:[Number], index:{type:'2dsphere', sparse:true}
});

reservaSchema.methods.diasdeReserva=function(){    
    return moment(this.desde).diff(moment(this.desde), 'day' )+1;
}

module.exports=mongoose.model('reserva', reservaSchema);
