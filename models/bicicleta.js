var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var BicicletaSchema=new Schema({  
    codigo: Number,
    color: String,
    modelo: String,
    ubicacion:{type:[Number], index:{type:'2dsphere', sparse:true}
}});
  

//metodo to string se cambia prototype por method
BicicletaSchema.method.toString= function(){
    return 'codigo: ' + this.codigo + ' |color: ' + this.color;
}


BicicletaSchema.static.allBicis=function(cb){
    return this.find({},cb);
}
BicicletaSchema.static.crearInstancia=function(codigo, color, modelo
    ,ubicacion){
        return new this({
            codigo:codigo,
            color:color,
            modelo:modelo,
            ubicacion:ubicacion
        })

}

BicicletaSchema.static.add=function(aBici, cb){
    this.create(aBici,cb);
}

BicicletaSchema.static.findByCode=function(aCode, cb){
    return this.findOne({code:aCode},cb);
}

BicicletaSchema.static.removeByCode=function(aCode,cb){
    return this.findOne({code:aCode},cb);
}

//var b1= new Bicicleta(1,'Rojo','Baccio',[-34.8833 ,-56.1667]);
//var b2= new Bicicleta(2,'Verde','GT',[-34.8833, -56.2697]);
module.exports=mongoose.model('Bicicleta', BicicletaSchema);


