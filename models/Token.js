var mongoose=require('mongoose');
var Schema=mongoose.Schema;



const tokenSchema=new Schema({  

    _UserId:{type:mongoose.Schema.Types.ObjectId, ref:'usuario'},
    token:{type: String, required:true},
    createAt:{type:Date, required:true, default:Date.now, expires:4200}
});

module.exports=mongoose.model('Token', tokenSchema);