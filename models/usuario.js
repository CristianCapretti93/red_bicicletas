var mongoose=require('mongoose');
var Schema=mongoose.Schema;
const validatorUnique=require('mongoose-unique-validator');
var Reserva=require('./reserva');
var moment=require('moment');
var crypto=require('crypto');
var bcrypt=require('bcrypt');
const Token = require('./Token');
const { getMaxListeners } = require('./reserva');
const mailer=require('../mailer/mailer');
const saltRounds=10;
const validarEmail=function(email){
    var regex= /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/
   if(regex.test(email))
   return; //test es el metodo regex que ya se tiene 
}

var usuarioSchema=new Schema({  
    nombre:{
        Type:String,
        trim:[true],
        required:[true, "El nombre debe ser obligatorio"]
    },
    email:{
        Type:String,
        trim:[true],
        required:[true, "El email debe ser obligatorio"],
        unique:true,
        lowercase:true,
        validate:[validarEmail, "por favor ingrese un email"]
    },
    password:{
        type:String,
        required:[true, "El password es obligatorio"]
    },
    passwordRestToken:String,
    passwordRestExpore:Date,
    verificado:{
        type:Boolean,
        default:false,
    }
});

usuarioSchema.pre('save', function(next)    //metodo que se utiliza antes de guardar el usuario
{
    if(this.isModified('pasword')){
        this.password=bcrypt.hashSync(this.password, saltRounds)
    }
    next();
})

usuarioSchema.plugin(validatorUnique, {message:"Existe un usuario coneste email"})

usuarioSchema.methods.reservar=function(biciId,desde,hasta,cb){
    var reserva= new Reserva({usuario: this._id, bicicleta: biciId, desde,hasta })
    console.log(reserva);
    reserva.save(cb);
}
usuarioSchema.methods.ValidarPasword=function(pass)
{
    return bcrypt.compareSync(pass,this.password)
}
usuarioSchema.methods.enviar_email_bienvenida=function(cb){
    const token=new Token({_UserId: this.id, token: crypto.randomBytes(16).toString('hex')})
    const email_destination=this.email;
    token.save(function(err){
        if(err)
        {
            return console.log(err.message);
        }
        const mailOption={
            from:'no-replay@bicicletas.com',
            to:email_destination,
            subject:'Benvenido!',
            text:'Email generico, por favor verificar su cuenta '+'http://http://localhost:3000/' + '\/token/confirmation\/'+token.token
        };
        mailer.sendMail(mailOption, function(err)
        {
            if(err){return console.log(err.message)}
            console.log('Se envio el email')
        })
    })


}
module.exports=mongoose.model('Usuario', usuarioSchema);
