const passport=require('passport');
const localStrategy=require('passport-local').Strategy;
const Usuario=require('../models/usuario');

//Revisar documentacion de passport

passport.use( new localStrategy[
    function(email, password,done){ 
        Usuario.findOne({email:email},function(err,usuario){
            if(err) return done(err)
            if(!usuario) return done(null, false,{ message:"usuario no existe"}); 
            if(!usuario.ValidarPasword(password)) return done(null,false,{message:"usuario incorrecto"} )
            return done(null, user)
        })
    }]);

passport.deserializeUser(function(id, cb){
        //una ves deseralizado el usuario obtiene el usuario de busqueda y loguea si hay errores
    Usuario.findById(id, function(err, usuario)
    {
        cb(err, usuario)
    });
})

//informarme sobre los callback y mongo
passport.serializeUser(function(user, cb){

    cb(null, user.id);

})


module.exports=passport;